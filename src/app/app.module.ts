import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListarComponent } from './Properties/listar/listar.component';
import { AnadirComponent } from './Properties/anadir/anadir.component';
import { MenuComponent } from './Properties/menu/menu.component';
import { LoginComponent } from './Properties/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {FormBuilder } from '@angular/forms';

import {HttpClientModule} from '@angular/common/http';

//import {NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    ListarComponent,
    AnadirComponent,
    MenuComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
    //NgbModule.forRoot()
  ],
  providers: [FormBuilder],
  bootstrap: [AppComponent,
              ListarComponent,
              AnadirComponent]
})
export class AppModule { }
