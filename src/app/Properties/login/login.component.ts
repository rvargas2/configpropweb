import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private listar:boolean = false;
  private menu:boolean = false;
  private login:boolean = true;
  private datosInvalidos:boolean = false;
  private usuarioVacio:boolean = false;
  private claveVacia:boolean = false;

  private usuario:string = "";
  private clave:string = "";

  loginForm:FormGroup;

  constructor(private ruoter:Router,private fb: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      usuario: new FormControl('', [Validators.required, Validators.minLength(2)]),
      clave: new FormControl('',[Validators.required, Validators.minLength(2)])
    });
  }

  ocultarInvalid(){
    this.datosInvalidos = false;
  }

  soapCall() {

    this.usuario = this.loginForm.controls.usuario.value;
    this.clave = this.loginForm.controls.clave.value;

    if(this.usuario == "" && this.clave == ""){
      this.usuarioVacio = true;
      this.claveVacia = true;
    }
    else if(this.clave == ""){
      this.usuarioVacio = false;
      this.claveVacia = true;
    }
    else if(this.usuario == ""){
        this.usuarioVacio = true;
        this.claveVacia = false;
    }
    else{

      this.usuarioVacio = false;
      this.claveVacia = false;

      const settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://10.2.179.11:8080/seg-webservice/SeguridadWebService",
        "method": "POST",
        "headers": {
          "cache-control": "no-cache",
          "postman-token": "8a6a0b4c-4ae8-4945-8e6f-25570fad1e15"
        },
        "data": "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://webservice.fravega.com.ar/'><soapenv:Header/><soapenv:Body><web:autenticarUsuario><!--Optional:--><Legajo>"+ this.usuario +"</Legajo><!--Optional:--><Contrasenia>"+ this.clave +"</Contrasenia></web:autenticarUsuario></soapenv:Body></soapenv:Envelope>"
      }
      var xmlhttp = new XMLHttpRequest();
      
      xmlhttp.open(settings.method,settings.url,true);
      xmlhttp.send(settings.data);
      
         xmlhttp.onreadystatechange =  () => {
           if (xmlhttp.readyState == 4) {
              if (xmlhttp.status == 200) {
                   const xml = xmlhttp.responseXML;
                   this.usuario = this.loginForm.controls.usuario.value;
                   this.listar = true;
                   this.menu = true;
                   this.ruoter.navigate(["listar"]);
                   this.login = false;
               }else{
                 this.datosInvalidos = true;
               }
           }
       }
    }
  }





}
