import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-anadir',
  templateUrl: './anadir.component.html',
  styleUrls: ['./anadir.component.css']
})
export class AnadirComponent implements OnInit {

  @Input() public nombreP: string;

  sucursales: any[];

  private nomb: string;
  private listar: boolean = false;
  private sucSeleccionadas = new Array;
  private nombreAccion: string = "Continuar";

  constructor() { }

  ngOnInit() {
    this.sucursales = [
      { numero: 1, nombre: 'Santa Fe' },
      { numero: 2, nombre: 'Santa Fe' },
      { numero: 3, nombre: 'Santa Fe' },
      { numero: 4, nombre: 'Santa Fe' },
      { numero: 5, nombre: 'Santa Fe' },
      { numero: 6, nombre: 'Santa Fe' },
      { numero: 7, nombre: 'Santa Fe' },
      { numero: 8, nombre: 'Santa Fe' },
      { numero: 9, nombre: 'Santa Fe' },
      { numero: 10, nombre: 'Santa Fe' },
      { numero: 11, nombre: 'Santa Fe' },
      { numero: 12, nombre: 'Santa Fe' },
      { numero: 13, nombre: 'Santa Fe' },
      { numero: 14, nombre: 'Santa Fe' },
      { numero: 15, nombre: 'Santa Fe' },
      { numero: 16, nombre: 'Santa Fe' },
      { numero: 17, nombre: 'Santa Fe' },
      { numero: 18, nombre: 'Santa Fe' },
      { numero: 19, nombre: 'Santa Fe' },
      { numero: 20, nombre: 'Santa Fe' },
      { numero: 21, nombre: 'Santa Fe' }
    ];
  }
  obtenerNombre(nombre: string) {
    nombre = this.nombreP;
  }

  listarSuc() {
    this.listar = !this.listar
  }

  agregarSucSeleccionada(numero: string, nombre: string) {
    this.sucSeleccionadas.push(numero, nombre);
  }

  ContinuarAgregar() {
    this.nombreAccion = "Finalizar";
  }

  finalizarAgregar() {
    this.nombreAccion = "Continuar";
  }

  generaTabla() {
    // Obtenemos la referencia del elemento body
    var body = document.getElementsByTagName("body")[0];
    // Creamos un elemento <table> y un elemento <tbody>
    var tabla = document.createElement("table");
    var tblHead = document.createElement("thead");
    var tblHeadTr = document.createElement("tr");
    var tblHeadTd = document.createElement("td");
    var textoCeldaH = document.createTextNode("Número");
    var tblHeadTd2 = document.createElement("td");
    var textoCeldaH2 = document.createTextNode("Nombre");
    tblHeadTd.appendChild(textoCeldaH);
    tblHeadTd2.appendChild(textoCeldaH2);
    tblHeadTr.appendChild(tblHeadTd);
    tblHeadTr.appendChild(tblHeadTd2);
    tblHead.appendChild(tblHeadTr);
    tabla.appendChild(tblHead);

    var tblBody = document.createElement("tbody");

    for (let x = 0; x < this.sucursales.length; x++) {
      var fila = document.createElement("tr");
      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(this.sucursales[x].numero);
      var celda2 = document.createElement("td");
      var textoCelda2 = document.createTextNode(this.sucursales[x].nombre);
      celda.appendChild(textoCelda);
      fila.appendChild(celda);
      celda2.appendChild(textoCelda2);
      fila.appendChild(celda2);
      tblBody.appendChild(fila);
    }

    // agregamos la hilera al final de la tabla (al final del elemento tblbody)
    tblBody.appendChild(fila);
    // posicionamos el <tbody> debajo del elemento <table>
    tabla.appendChild(tblBody);
    // appends <table> into <body>
    body.appendChild(tabla);
    // modifica el atributo "border" de la tabla y lo fija a "2";
    tabla.setAttribute("border", '1');
    tabla.setAttribute("id", "tabla");
    var result = document.getElementById("resultado");
    result.appendChild(tabla);
  }

}
