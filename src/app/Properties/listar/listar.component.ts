import { Component, OnInit, Input } from '@angular/core';
import { SeviceLoginService } from 'src/app/Service/sevice-login.service';
import { Properties } from 'src/app/Model/Properties';
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  private properties:Properties = new Properties;
  private listProperties:Properties[] = new Array;
  private resultSuc:string[];

  private listar: boolean = false;

  private isavailable: boolean = false;
  private result: boolean = true;

  private checked: string = "truee";
  private alinear: string = "center";

  private indiceFila: number;

  private _service: SeviceLoginService;

  constructor(private serv:SeviceLoginService) {

  }

  ngOnInit() {
    this.properties.query = "";
    this.properties.numero_sucursales = [1];  
    this.serv.getProperties(this.properties).subscribe(prop => {
      this.listProperties = prop;
      console.log("Propiedades----------->" + this.listProperties[1].propertyName);
    });
    
  }

  myClickFunction(event) {
    this.isavailable = true;
  }

  obtenerFila(indice: number) {
    this.indiceFila = indice;
  }

  borrarFila() {
    if (this.indiceFila != null) {
      
    }
    this.indiceFila = null;
  }

}
