import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  title = 'ProyectoProperties';
  private listar:boolean = false;
  private anadir:boolean = false;

  correcto:boolean = false;

  constructor(private router: Router) {}

  Listar(){
    this.anadir = false;
    this.router.navigate(["/listar"]);
    this.listar = true;
  }

  Anadir(): void{
    this.listar = false;
    this.router.navigate(["add"]);
    this.anadir = true;
  }

  CerrarD(){
    this.correcto  = false;
  }

}
