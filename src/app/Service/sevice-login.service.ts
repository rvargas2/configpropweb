import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Properties } from 'src/app/Model/Properties';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SeviceLoginService {

  constructor(public http: HttpClient) { }

  private url: string = "http://10.32.9.56:9093/configuradorweb";

  getProperties(properties: Properties): Observable<Properties[]> {
    let json = JSON.stringify(properties);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    this.url = this.url+'/query';
    return this.http.post<Properties[]>(this.url, json, { headers: headers });

  }
}
