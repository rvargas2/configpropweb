import { TestBed } from '@angular/core/testing';

import { SeviceLoginService } from './sevice-login.service';

describe('SeviceLoginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SeviceLoginService = TestBed.get(SeviceLoginService);
    expect(service).toBeTruthy();
  });
});
